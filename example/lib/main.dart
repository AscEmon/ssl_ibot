import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:ssl_ibot/model/customer_information.dart';
import 'package:ssl_ibot/ssl_ibot.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final TextEditingController _nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Flutter SSL IBot'),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                // initialValue: "John Doe",
                controller: _nameController,
                keyboardType: TextInputType.text,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8.0))),
                  hintText: "Customer Name",
                ),
                validator: (value) {
                  if (value != null) {
                    return "Please input Customer name";
                  } else {
                    return null;
                  }
                },
              ),
            ),
            ElevatedButton(
              child: const Text("Load IBot"),
              onPressed: () {
                if (_nameController.text.isNotEmpty) {
                  loadiBotSdk();
                }
              },
            )
          ],
        ),
      ),
    );
  }

  Future<void> loadiBotSdk() async {
    SslIbot ssl = SslIbot(
      customerInformation: CustomerInformation(name: _nameController.text,email: "sh@gmail",clientToken: "63"),
    );
    ssl.loadIBot();
  }
}
