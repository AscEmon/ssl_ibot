import Flutter
import UIKit
import iBotSDK


public class SwiftSslIbotPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "ssl_ibot", binaryMessenger: registrar.messenger())
    let instance = SwiftSslIbotPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    result("iOS " + UIDevice.current.systemVersion)
   
    let arguments = call.arguments 
    print(arguments)
  }
}
