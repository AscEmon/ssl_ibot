#import "SslIbotPlugin.h"
#if __has_include(<ssl_ibot/ssl_ibot-Swift.h>)
#import <ssl_ibot/ssl_ibot-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "ssl_ibot-Swift.h"
#endif

@implementation SslIbotPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftSslIbotPlugin registerWithRegistrar:registrar];
}
@end
