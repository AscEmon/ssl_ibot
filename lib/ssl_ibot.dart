import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:ssl_ibot/model/customer_information.dart';

@JsonSerializable(explicitToJson: true)
class SslIbot {
  CustomerInformation customerInformation =
      CustomerInformation(name: "", clientToken: "");

  //***********************/
  SslIbot({required CustomerInformation customerInformation}) {
    this.customerInformation = customerInformation;
  }

  static const MethodChannel _channel = MethodChannel('ssl_ibot');

  Future<dynamic> loadIBot() async {
    try {
      var response = await _channel.invokeMethod(
        'call_ibotSdk',
        jsonEncode(customerInformation),
      );
      print("After jsonencode");
    } on PlatformException catch (e) {
      debugPrint(e.toString());
      return e;
    }
  }
}
