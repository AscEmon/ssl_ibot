import 'package:json_annotation/json_annotation.dart';
part 'customer_information.g.dart';


@JsonSerializable()
class CustomerInformation {
  String name;
  String? phone;
  String? email;
  String? profilePicUrl;
  String clientToken; 

  CustomerInformation(
      {required this.name, this.email, this.phone, this.profilePicUrl,required this.clientToken});

Map<String, dynamic> toJson() => _$CustomerInformationToJson(this);

  factory CustomerInformation.fromJson(Map<String, dynamic> json) =>
      _$CustomerInformationFromJson(json);

  String getName() {
    return this.name;
  }

  void setName(String name) {
    this.name = name;
  }

  String? getEmail() {
    return this.email;
  }

  void setEmail(String email) {
    this.email = email;
  }

  String? getPhone() {
    return this.phone;
  }

  void setPhone(String phone) {
    this.phone = phone;
  }

  String? getProfilePicUrl() {
    return this.profilePicUrl;
  }

  void setProfilePicUrl(String profilePicUrl) {
    this.profilePicUrl = profilePicUrl;
  }
    String? getClientToken() {
    return this.clientToken;
  }

  void setClientToken(String clientToken) {
    this.clientToken = clientToken;
  }
}
