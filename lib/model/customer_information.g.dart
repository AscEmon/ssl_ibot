// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_information.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerInformation _$CustomerInformationFromJson(Map<String, dynamic> json) {
  return CustomerInformation(
    name: json['name'] as String,
    email: json['email'] as String?,
    phone: json['phone'] as String?,
    profilePicUrl: json['profilePicUrl'] as String?,
    clientToken: json['clientToken'] as String
  );
}

Map<String, dynamic> _$CustomerInformationToJson(
        CustomerInformation instance) =>
    <String, dynamic>{
      'name': instance.name,
      'phone': instance.phone,
      'email': instance.email,
      'profilePicUrl': instance.profilePicUrl,
      'clientToken':instance.clientToken
    };
